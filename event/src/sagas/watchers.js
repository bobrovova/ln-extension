import { takeEvery, select } from 'redux-saga/effects';
import store from 'Store';
import { setPage } from 'Actions';
import { eventChannel, END } from 'redux-saga'

const getPaymentOfProof = state => state.app.get('paymentOfProof');

function* watchPaymentHeader(action){
    chrome.webRequest.onHeadersReceived.addListener(function(details){
            details.responseHeaders.forEach(function(v,i,a){
                if(v.name == "X-Payment-Page" && v.value == "true"){
                    store.dispatch(setPage('payment'));
                }
            });
        },
        {urls: ["http://*/*"]},
        ["responseHeaders"]
    );
}

function* watchSendHeaders(action){
    chrome.webRequest.onBeforeSendHeaders.addListener(
        function(details){
            let paymentOfProof = store.getState().app.get('paymentOfProof');
            if(paymentOfProof == null){
                paymentOfProof = 'null';
            }
            details.requestHeaders[details.requestHeaders.length] = {
                name: 'X-Payment-Proof',
                value: paymentOfProof
            }
            return { requestHeaders: details.requestHeaders }
        },
        {urls: ["<all_urls>"]},
        ["blocking", "requestHeaders"]
    );
}

export default function* watchers(){
    yield takeEvery("WATCH_PAYMENT_HEADER", watchPaymentHeader);
    yield takeEvery("WATCH_SEND_HEADERS", watchSendHeaders);
}
